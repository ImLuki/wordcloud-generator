#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import os

from web import webpage

amount = 5

if __name__ == '__main__':
    for file in os.listdir(".\\ressources\\uploads"):
        os.remove(f".\\ressources\\uploads\\{file}")
    webpage.start()
