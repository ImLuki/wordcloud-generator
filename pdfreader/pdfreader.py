import collections
from tika import parser
from textparser import textparser

WORDS_TO_REMOVE = ["der", "die", "das", "und", "ist", "da", "für", "ein", "ein", "den", "dem", "auf",
                   "bei", "dieser", "nicht", "in", "zu", "für", "zu", "auf", "im", "werden"]


def get_text(path: str):
    raw = parser.from_file(path)
    return textparser.prepare_words(raw['content'])


def get_dictionary_from_pdf(path: str):
    text = get_text(path=path)
    dic = collections.defaultdict(int)
    for word in text.split():
        dic[textparser.fix(text=word)] += 1  # TODO prepare-words?

    return remove_words_from_dic(dic=dic)


def remove_words_from_dic(dic: dict):
    for word in WORDS_TO_REMOVE:
        dic.pop(word, None)

    return dic
