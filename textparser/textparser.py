import re


def fix(text: str):
    text = text.replace('¨a', 'ä')
    text = text.replace('¨o', 'ö')
    text = text.replace('¨u', 'ü')
    text = text.replace('¨A', 'Ä')
    text = text.replace('¨O', 'Ö')
    text = text.replace('¨U', 'Ü')
    text = text.replace('\\', '')
    # text = text.replace('�', 'fi')  # TODO this better

    # remove chars from string
    text = re.sub('[/.;:,"\r\n]', '', text)
    return text


def prepare_words(word: str):
    return fix(word.lower()).strip()

# TODO add list with words to delete

# TODO CS GRID
