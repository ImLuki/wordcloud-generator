#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import collections

import codecs
from os import listdir

from textparser import textparser


def get_words():
    dic = collections.defaultdict(int)
    with codecs.open('web\\static\\ressources\\uploads\\words.txt', 'r', encoding='utf8') as f:
        for word in f.readlines():
            result = word.split(";")
            if len(result) == 2:
                dic[textparser.prepare_words(word=result[0])] += int(result[1])
            elif len(result) == 1:
                dic[textparser.prepare_words(word=word)] += 1

    return dic


def save_words(words, path='web\\static\\ressources\\uploads\\words.txt'):
    output = []
    for key in words:
        output.append(f"{key};{words[key]}\n")
    output = sorted(output)
    f = open(path, 'w', encoding='utf8')
    f.write(''.join(output)[:-1])
    f.close()


def get_image_list():
    files = [f for f in listdir(".\\web\\static\\images")]
    files = sorted(files)
    return [{
        "path": f"\\static\\images\\{files[i]}",
        "number": str(i + 1)
    } for i in range(len(files))]
