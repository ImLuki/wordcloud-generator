import os
import random
import threading
import webbrowser
from os import listdir

from flask import Flask, render_template, request, redirect

from pdfreader.pdfreader import get_dictionary_from_pdf
from util.ressource_loader import get_words, get_image_list, save_words
from wordcloudgeneration.colormapmanager import COLORS
from wordcloudgeneration.image_generator import ImageGenerator

basedir = os.path.abspath(os.path.dirname(__file__))
UPLOAD_FOLDER = '.\\web\\static\\ressources\\uploads'
IMAGE_FOLDER = ".\\web\\static\\images"
ALLOWED_EXTENSIONS = {'txt', 'pdf'}
IMAGE_EXTENSIONS = {"png", "jpg"}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

try:
    image_generator = ImageGenerator(get_words())
except:
    image_generator = None


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def image_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in IMAGE_EXTENSIONS


@app.route('/')
def home():
    return redirect('/upload')


@app.route('/words')
def handle_words():
    data = []
    dic = get_words()
    for key in dic:
        data.append({
            "word": key,
            "amount": dic[key]
        })
    return render_template("words.html", data=data)


@app.route('/display')
def display():
    global image_generator
    paths = get_image_list()
    colors = COLORS.copy()
    colors.append("None")
    if image_generator.data["color1"] != "None":
        colors.remove(image_generator.data["color1"])
    colors.remove(image_generator.data["color2"])
    return render_template("display.html", images=paths, image_count=len(paths), data=image_generator.data,
                           word_list=image_generator.sorted_word_list, refresh_number=image_generator.refresh_number,
                           colors=colors)


@app.route('/upload')
def upload():
    return render_template('upload.html')


@app.route('/upload', methods=['POST', 'GET'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            print("no file")
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return redirect(request.url)
        if file and allowed_file(file.filename):
            if file.filename.endswith("pdf"):
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
                dic = get_dictionary_from_pdf(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
                save_words(words=dic)
            else:
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], "words.txt"))
            words = get_words()
            save_words(words=words, path=os.path.join(app.config['UPLOAD_FOLDER'], "words.txt"))
            for file in listdir(IMAGE_FOLDER):
                os.remove(f"{IMAGE_FOLDER}\\{file}")
            global image_generator
            image_generator = ImageGenerator(words=words)
            image_generator.get_first_image()
            return redirect("display")


@app.route('/more_images')
def more_images():
    global image_generator
    image_generator.process_more_images()
    return redirect('/display')


@app.route('/remove-word/<word>', methods=['POST'])
def remove_word(word):
    global image_generator
    image_generator.remove_word(word)
    return redirect('/display')


@app.route('/commit_deleted_words')
def commit_deleted_words():
    global image_generator
    save_words(words=image_generator.words)
    image_generator.reset()
    return redirect('/display')


@app.route('/more_images_plus')
def more_images_plus():
    global image_generator
    image_generator.process_more_images_plus()
    return redirect('/display')


@app.route('/update_data', methods=["post"])
def update_data():
    color = request.form['background_color']
    height = int(request.form['height'])
    width = int(request.form['width'])
    color1 = request.form["color1"]
    color2 = request.form["color2"]
    relative_scale = float(request.form['scale'])
    max_font_size = int(request.form['max_font_size'])
    custom_color = request.form["custom_color"]
    if custom_color != "":
        color1 = "None"
        color2 = "None"
    else:
        if color1 == "None":
            color1 = "summer"
    if 'form' in request.files:
        file = request.files['form']
        form = handel_form_input(file=file)
    else:
        form = "None"
    image_generator.update_data(color=color, height=height, width=width, color1=color1, color2=color2, form=form,
                                relative_scale=relative_scale, custom_color=custom_color, max_font_size=max_font_size)
    return redirect('/display')


def handel_form_input(file):
    filename = file.filename
    if file.filename == '' or not image_file(filename=file.filename):
        return "None"
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return f"{UPLOAD_FOLDER}//{filename}"


@app.route('/delete_image', methods=["post"])
def delete_image():
    image_id = int(request.form['image_id']) - 1
    rename = False
    files = listdir(IMAGE_FOLDER)

    if len(files) <= 1:
        image_generator.reset_generation(index=0)
        for file in files:
            os.remove(f"{IMAGE_FOLDER}\\{file}")
        image_generator.index = 0
        image_generator.get_first_image()

    else:

        image_generator.reset_generation(index=image_generator.current_image)
        for i in range(len(files)):

            if i > image_generator.current_image:
                os.remove(f"{IMAGE_FOLDER}\\{files[i]}")
                continue

            if rename:
                os.rename(f"{IMAGE_FOLDER}\\{files[i]}",
                          f"{IMAGE_FOLDER}\\{image_generator.refresh_number}_image{i - 1:03}.png")
                continue

            if files[i] == f"{image_generator.refresh_number - 1}_image{image_id:03}.png":
                rename = True
                os.remove(f"{IMAGE_FOLDER}\\{image_generator.refresh_number - 1}_image{image_id:03}.png")
                continue

            os.rename(f"{IMAGE_FOLDER}\\{files[i]}",
                      f"{IMAGE_FOLDER}\\{image_generator.refresh_number}_image{i:03}.png")

        image_generator.process_more_images()

    return redirect('/display')


def start():
    for path in [UPLOAD_FOLDER, IMAGE_FOLDER]:
        try:
            os.mkdir(path)
        except OSError:
            print("Creation of the directory %s failed" % path)
        else:
            print("Successfully created the directory %s " % path)

    port = 5000 + random.randint(0, 999)
    url = f"http://127.0.0.1:{port}"

    threading.Timer(1.25, lambda: webbrowser.open(url)).start()
    app.run(port=port, debug=False)
