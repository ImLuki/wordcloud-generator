import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

COLORS = ['Accent', 'afmhot', 'autumn', 'binary', 'Blues', 'bone', 'BrBG', 'brg', 'BuGn', 'BuPu', 'bwr', 'CMRmap',
          'cool', 'coolwarm', 'copper', 'cubehelix', 'Dark2', 'flag', 'gist_earth', 'gist_gray', 'gist_heat',
          'gist_ncar', 'gist_rainbow', 'gist_stern', 'gist_yarg', 'GnBu', 'gnuplot', 'gnuplot2', 'gray', 'Greens',
          'Greys', 'hot', 'hsv', 'inferno', 'jet', 'magma', 'nipy_spectral', 'ocean', 'Oranges', 'OrRd', 'Paired',
          'Pastel1', 'Pastel2', 'pink', 'PiYG', 'plasma', 'PRGn', 'prism', 'PuBu', 'PuBuGn', 'PuOr', 'PuRd', 'Purples',
          'rainbow', 'RdBu', 'RdGy', 'RdPu', 'RdYlBu', 'RdYlGn', 'Reds', 'seismic', 'Set1', 'Set2', 'Set3', 'Spectral',
          'spring', 'summer', 'tab10', 'tab20', 'tab20b', 'tab20c', 'terrain', 'viridis', 'winter', 'Wistia', 'YlGn',
          'YlGnBu', 'YlOrBr', 'YlOrRd']


def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=-1):
    if n == -1:
        n = cmap.N
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({name},{a:.2f},{b:.2f})'.format(name=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap


def get_combined_map(color1: str, color2: str, custom_color: str):
    custom_colors = [f"#{c}" for c in custom_color.split(";")]
    if custom_color != "":
        return mcolors.ListedColormap(custom_colors)
    if color2 == "None":
        colors = plt.get_cmap(color1)(np.linspace(0, 1, 256))
        """
        minColor = 0.00
        maxColor = 0.85

        parameterToColorBy = np.linspace(5, 10, 6, dtype=float)
        inferno_t = truncate_colormap(plt.get_cmap(color1), minColor, maxColor)

        colors = [inferno_t(i) for i in np.linspace(0, 1, parameterToColorBy.shape[0])]
        """
    else:
        c1 = plt.get_cmap(color1)(np.linspace(0, 1, 128))
        c2 = plt.get_cmap(color2)(np.linspace(0, 1, 128))
        colors = np.vstack((c1, c2))
    return mcolors.LinearSegmentedColormap.from_list('my_colormap', colors)
