import multiprocessing
import os

import psutil

from wordcloudgeneration.wordcloudgenerator import generate_image

PRE_GENERATED_IMAGE_COUNT = max(os.cpu_count() - 2, 2)


class ImageGenerator:

    def __init__(self, words) -> None:
        self.index = 0
        self.refresh_number = 0
        self.words = words
        self.sorted_word_list = self.sort_word_list()
        self.processes = []
        self.data = get_data()
        self.current_image = 0

    def get_first_image(self):
        generate_image(words=self.words, image_number=self.index, data=self.data, refresh_number=self.refresh_number)
        self.index += 1

        process = multiprocessing.Process(target=self.create_processes)
        process.start()
        self.processes.append(process)
        self.index += PRE_GENERATED_IMAGE_COUNT

    def create_processes(self):
        for _ in range(PRE_GENERATED_IMAGE_COUNT):
            process = multiprocessing.Process(target=generate_image,
                                              args=(self.words, self.index, self.data, self.refresh_number))
            process.start()
            self.index += 1
            self.processes.append(process)

        for process in self.processes:
            process.join()
            self.processes.remove(process)

    def process_more_images(self):
        for process in self.processes:
            process.join()
            self.processes.remove(process)

        self.current_image += PRE_GENERATED_IMAGE_COUNT

        process = multiprocessing.Process(target=self.create_processes)
        process.start()
        self.processes.append(process)
        self.index += PRE_GENERATED_IMAGE_COUNT

    def process_more_images_plus(self):
        amount = 50 - PRE_GENERATED_IMAGE_COUNT

        for process in self.processes:
            process.join()
            self.processes.remove(process)
        self.current_image += PRE_GENERATED_IMAGE_COUNT
        while amount > 0:

            for _ in range(PRE_GENERATED_IMAGE_COUNT):
                process = multiprocessing.Process(target=generate_image,
                                                  args=(self.words, self.index, self.data, self.refresh_number))
                process.start()
                self.processes.append(process)
                self.index += 1
                amount -= 1
                self.current_image += 1
                if amount <= 0:
                    break
            for process in self.processes:
                process.join()
                self.processes.remove(process)

        process = multiprocessing.Process(target=self.create_processes)
        process.start()
        self.processes.append(process)
        self.index += PRE_GENERATED_IMAGE_COUNT

    def sort_word_list(self):
        data = []
        for key in self.words:
            data.append({
                "word": key,
                "amount": self.words[key]})
            data.sort(key=lambda i: i['word'])
            data.sort(key=lambda i: i['amount'], reverse=True)
        return data

    def update_data(self, color: str, height: int, width: int, color1: str, color2: str, form: str,
                    relative_scale: float, custom_color: str, max_font_size: int):
        if self.data["color"] == color \
                and self.data["width"] == width \
                and self.data["height"] == height \
                and self.data["color1"] == color1 \
                and self.data["color2"] == color2 \
                and self.data["form"] == form \
                and self.data["custom_color"] == custom_color \
                and self.data["scale"] == relative_scale \
                and self.data["max_font_size"] == max_font_size:
            print("no changes")
            return

        self.data["color"] = color
        self.data["height"] = height
        self.data["width"] = width
        self.data["color1"] = color1
        self.data["color2"] = color2
        self.data["form"] = form
        self.data["custom_color"] = custom_color
        self.data["scale"] = relative_scale
        self.data["max_font_size"] = max_font_size

        self.reset()

    def reset(self):
        self.reset_generation()

        for file in os.listdir(".\\web\\static\\images"):
            os.remove(f".\\web\\static\\images\\{file}")
        self.get_first_image()

    def remove_word(self, word):
        self.words.pop(word)
        self.sorted_word_list = self.sort_word_list()

    def reset_generation(self, index=0):
        p = multiprocessing.Process()
        p.is_alive()
        for process in self.processes:
            if process.is_alive():
                for child in psutil.Process(process.pid).children(recursive=True):
                    child.terminate()
            process.terminate()
        self.refresh_number += 1
        self.index = index
        self.current_image = index
        self.processes = []


def get_data():
    return {
        "color": "FFFFFF",
        "width": 1920,
        "height": 1080,
        "color1": "viridis",
        "color2": "None",
        "font": "cambria",
        "form": "None",
        "custom_color": "",
        "scale": 0.0,
        "max_font_size": 0
    }
