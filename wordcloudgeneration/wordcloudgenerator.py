import numpy as np
import wordcloud
import matplotlib.pyplot as plt
from wordcloudgeneration import colormapmanager
from PIL import ImageColor, Image

FONT_PATH = "\\ressources\\fonts\\{}.ttc"
COLOR_MAP = "autumn"
DPI = 200


def generate_word_cloud(dictionary: dict, image_number: int, data, refresh_number):

    relative_scale = data['scale']
    if relative_scale == 0:
        relative_scale = 'auto'

    max_font_size = data['max_font_size']
    if max_font_size == 0:
        max_font_size = None
    print(data['max_font_size'])

    if data["form"] != "None":

        mask = np.array(Image.open(data["form"]))
        plt.figure(figsize=(len(mask[0]) / DPI, len(mask) / DPI), dpi=DPI)
        cloud = wordcloud.WordCloud(mask=mask,
                                    max_font_size=max_font_size,
                                    relative_scaling=relative_scale,
                                    font_path=FONT_PATH.format(data["font"]),
                                    background_color=ImageColor.getrgb(f"#{data['color']}"),
                                    colormap=colormapmanager.get_combined_map(color1=data["color1"], color2=data[
                                        "color2"], custom_color=data['custom_color']))
    else:
        plt.figure(figsize=(data["width"] / DPI, data["height"] / DPI), dpi=DPI)
        cloud = wordcloud.WordCloud(width=data["width"], height=data["height"],
                                    max_font_size=max_font_size,
                                    relative_scaling=relative_scale,
                                    font_path=FONT_PATH.format(data["font"]),
                                    background_color=ImageColor.getrgb(f"#{data['color']}"),
                                    colormap=colormapmanager.get_combined_map(color1=data["color1"], color2=data[
                                        "color2"], custom_color=data['custom_color']))

    cloud.generate_from_frequencies(dictionary)
    plt.imshow(cloud, interpolation='bilinear')
    plt.axis("off")
    plt.tight_layout(pad=0)
    plt.savefig(f"web\\static\\images\\{refresh_number}_image{image_number:03}.png")
    # plt.savefig(f"image{image_number}.svg", bbox_inches='tight')
    plt.close()
    print(f"image {image_number} done.")


def generate_image(words: dict, image_number, data, refresh_number):
    generate_word_cloud(words, image_number, data=data, refresh_number=refresh_number)


def transform_format(val):
    if val == 0:
        return 255
    else:
        return val
